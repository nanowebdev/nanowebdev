# NanoWebDev
[![Netlify Status](https://api.netlify.com/api/v1/badges/a6a670e6-72e2-45f0-8ca2-603d9e9337ec/deploy-status)](https://app.netlify.com/sites/nanowebdev/deploys)


Build small applications for horizontal scaling purpose.

Visit: [https://nanowebdev.netlify.app](https://nanowebdev.netlify.app)
