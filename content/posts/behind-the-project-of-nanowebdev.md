---
title: "Behind The Project of NanoWebDev"
date: 2022-11-02T20:57:26+07:00
lastmod: 2022-11-02T20:57:26+07:00
draft: false
author: "aalfiann"
authorLink: "https://linktr.ee/aalfiann"
authorEmail: "aalfiann@gmail.com"
keywords: "Behind The Project of NanoWebDev"
images: ["/img/content/behind-the-project.jpg"]

tags: ["development", "code-of-conduct"]
categories: ["Documentation"]
featuredImage: "/img/content/behind-the-project.jpg"
---

This article will inform you about behind the project of NanoWebDev. So the new developer can easily understand what should to do before getting started to develop our project.

<!--more-->

## What we create ?
Our goal is to create an applications that scalable for horizontal scaling purpose. We create a small isolated application so our application can run standalone and could connect to other applications via API. When we setup our applications run as service, then we can call this `Microservices`. 

## How we create ?
There is a rules to make a microservices application:
1. Application must be `small` and can run `standalone`.
2. Application support to run in `container services` like docker.
3. Application support to run in `cluster` mode (multi thread).
4. Application cache could switch easily from `memory` to using `Redis` when in cluster or scale mode.
5. Authentication and Authorization must be `stateless` (no session and cookie).
6. Application have `own database`, never connect directly on others database.
7. Communication with other applications should using Rest `API`.
8. If the application have an upload feature, never upload to same directory application. File should be uploaded to another server storage or storage service like AWS S3, etc.

## Code of Conduct
NanoWebDev using [Fastify](https://fastify.io) framework which is powered by NodeJS and written using JavaScript programming language.  

Server (backend):
- Minimum [NodeJS v14.19.3](https://nodejs.org/download/release/v14.19.3/).
- NodeJS Framework [Fastify v4](https://fastify.io).
- Use strict.
- Rest API first.
- API Documentation using [Postman](https://www.postman.com).
- Default Database is `SQLite` but have to support switch to multiple RDBMS. i.e MySQL, MariaDB and PostgreSQL.
- Default `cache server` is using `memory` but have to support switch to [Redis](https://redis.io) easily.
- Everything is Stateless using [JWT](https://jwt.io) (no session and cookie).
- Dockerized to support deploy on container service.
- Codestyle [StandardJS](https://standardjs.com)

Views (frontend):
- Use strict.
- Using [ETA](https://eta.js.org) template engine.
- CSS Framework using [Bootstrap 5](https://getbootstrap.com).
- Default theme template using [Argon Dashboard 2 - Free Version](https://www.creative-tim.com/product/argon-dashboard).
- Support for all modern browser with minimum EDGE browser (no IE support).
- Vanilla JavaScript (no Jquery).

## Get Started
If you interested to develop our project. You can start by learning one of our project source code. Applications is very small, so you will be easy to read, learn and understanding the source code.

## Update
Currently NanoWebDev develop our applications based on above direction. We could change our direction in the future following the new technology or new solution that more efficient.
