---
title: "How to Integrate Open SSO"
date: 2022-11-30T18:58:26+07:00
lastmod: 2022-11-30T18:58:26+07:00
draft: false
author: "aalfiann"
authorLink: "https://linktr.ee/aalfiann"
authorEmail: "aalfiann@gmail.com"
keywords: "How to Integrate Open SSO"

tags: ["opensso", "opensso-docs"]
categories: ["Documentation"]
---
Open SSO can be use for your external user management or simply just use Open SSO for authenticate your users in your existing application.  
<!--more-->

#### Table Of Contents
**[Integrate Open SSO](#integrate-open-sso)**
  - [Diagram Flow](#diagram-flow)
  - [Steps Integration](#steps-integration)
  - [Example or Starter Template](#example-or-starter-template)
  - [F.A.Q](#faq)

#### Learn more
- **[Open SSO Get Started](/posts/open-sso-documentation)**
- **[Open SSO API Documentation](/posts/open-sso-api-documentation)**
- **[Open SSO Advanced Configuration](/posts/open-sso-advanced-configuration)**
---

## Integrate Open SSO
This article will explain how to integrate Open SSO to your new project or existing application which is already have their own user database.


## Diagram Flow
To integrate Open SSO, it will require a little programming knowledge.  
For more detail, you can see the flow/diagram about how to integrate Open SSO. [Click here to see the larger diagram](/img/content/doc-opensso/open-sso-flow.png).  

**Integration Open SSO to Existing Application Diagram**  

![](/img/content/doc-opensso/open-sso-flow-compressed.png)  

> [Back to top ^](#table-of-contents)
---


## Steps Integration
Here is the simple steps to integrate Open SSO.

**A. Get SSO URL Login Page**

1. Register an account then Login.
2. Go to menu `My SSO`
3. Add new `SSO`  
![](https://i.imgur.com/XccWQAs.png)
4. Click Submit
5. Done, then you are able to put the `SSO Url` into your website.  
![](https://i.imgur.com/8ZyAUbC.png)
6. Now try to login with our Open SSO system by visiting the `SSO Url` from your website.

**Note:**
- Field Name is your application or website name.
- Field Callback is the url to redirect user back to your website including the token. 
- When you update, the `Key` and `Callback Url` will always change.

> [Back to top ^](#table-of-contents)
---


**B. Create Button**  
Create a button to your website.

```html
<button class="btn btn-sm btn-primary"
  onclick="location.href='https://sso.yourdomain.com/sso/login/xxxxxxxxxx'">
  Single Sign On
</button>
```
The button will redirect user to the SSO URL Login Page. User will required to fill the form login.

> [Back to top ^](#table-of-contents)
---


**C. Validate JWT Token**  
Once Open SSO redirect to your callback page. You have to validate the JWT token from Open SSO.  

Here is the example function to decoded the JWT token in JavaScript.
```javascript
function parseJWT (token) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
  return JSON.parse(jsonPayload);
};
```

Here is the example output of decoded JWT token.
```json
{
	"uid": "f1d45f22-e5e9-4077-915e-0278fc578xxx",
	"unm": "admin",
	"name": "admin",
	"mail": "admin@youremail.com",
	"role": "admin",
	"gravatar": "https://gravatar.com/avatar/0e1aa649b4ffff2a7b61e3b7d672b522",
	"hash": "A25NTbjoG1pphZzNjMTI",
	"iat": 1688552848,
	"exp": 1688581648
}
```

**Note:**
- `uid` is the user id of user.
- `unm` is the username of user.
- `iat` is identifies the time at which the JWT was issued.
- `exp` is expiration time for JWT.
- `mail` is the email of user.
- `role` is the level of user, only used in Open SSO and only has two `admin` and `member`.
- `gravatar` is the global avatar of user.
- `hash` is the random generated string, only used for internal system.
- It's better to validate on server side than on client side. Actualy you can use any libraries to work with JWT Token.

> [Back to top ^](#table-of-contents)
---


**D. Check User on Existing Database**  
If the token successfully validated, then you should check, is the user exist or not?  
If there is no user exists, then you should register and activate it silently with user information from JWT Token to your existing application.

> [Back to top ^](#table-of-contents)
---


**E. Authenticate / Allow Session**  
The last steps is you should giving authenticate or allow session to the user. Done.

> [Back to top ^](#table-of-contents)
---


## Example or Starter Template
We offer the starter template or example scripts that already integrated with OpenSSO into your new static html or new web application.  
This will saving many hours just for creating a new application.

**Native:**  
- [Static HTML or Client Side (Pure HTML + JS)](https://gitlab.com/nanowebdev/example/opensso/client-side)
- [Native PHP](https://gitlab.com/nanowebdev/example/opensso/native-php)

**Framework:**  
- [Laravel v8](https://gitlab.com/nanowebdev/example/opensso/laravel-8)
- [TotalJS v4](https://gitlab.com/nanowebdev/example/opensso/totaljs-4)
- [ExpressJS v4](https://gitlab.com/nanowebdev/example/opensso/expressjs-4)
- [FastifyJS v4](https://gitlab.com/nanowebdev/example/opensso/fastify-4)
- [NestJS v10](https://gitlab.com/nanowebdev/example/opensso/nestjs-10)
- [NextJS v13](https://gitlab.com/nanowebdev/example/opensso/nextjs-13)

**Note:**  
- BYOB - Bring Your Own Database, the starter template is not included with Database.
- The starter template was created with very minimalist so you are easier to learn.
- I'll update to add more example script in the future.

> [Back to top ^](#table-of-contents)
---


## F.A.Q
Here is the Frequent Answered Question about Integration Open SSO.  

**1. Is this approach safe?**  
Yes, we follow the JWT standard `Oauth 2.0` as describe in [RFC7519](https://www.rfc-editor.org/rfc/rfc7519) and [RFC9068](https://datatracker.ietf.org/doc/html/rfc9068).  

**2. Is Open SSO will work for mobile or any application?**  
Yes, of course. As long as you can integrate the Open SSO and manage the JWT token. There are plenty JWT libraries out there. Please see [here](https://jwt.io/libraries).

**3. Is it easy to Integrate Open SSO?**  
To integrate Open SSO, it will be require a little programming skill.  
I would say it easy if:
- You ever made a application with user management system including login and logout.
- You ever made Login via Google or Login with Facebook.

If you don't have any programming skill, you should hire the freelancer that have knowledge about NodeJS, JWT and programming language that used in your application.

**4. Why the roles only admin and member in Open SSO?**  
Open SSO only authenticate user, not giving authorization to the user. Authorization must be applicable on the external application.

**5. When I logout on my application, how to make my account also logout on Open SSO?**  
Actualy there is no way to make user logout on Open SSO from external application, Because the JWT token is saved on their device or external application, not in the Open SSO server. This is the benefit of using JWT, because it's stateless.  

For example, there many websites that using facebook login, if you logout from that website, your facebook account still logged in.

Note:
- Open SSO JWT token will expired automatically in 8 hours as default.

**6. I have user login system running already, can Open SSO work as optional user login?**  
Yes of course, Open SSO has own user database that different with your existing application.

**7. I have thousand users, is it possible to integrate SSO that would work for old registered user too?**  
Simple answer is, it can not. You are able to integrate Open SSO to your existing application. But it would work for new registered user only. The reason is, Open SSO has own user database and it is should be isolated.  

Actual answer is, it possible, but yes, it would require complicated technical task.

**8. I have old user login system, can I switch to only use Open SSO login in the future?**  
Yes it is possible, but this will require a complicated task and maybe you will lost your users if your user lost their email (there are possibility that some people using fake email in the past).  

The steps is:
  - You have to import the user data from your current application to Open SSO user database.
  - You have to inform to all users to request new password via `forgot password` on Open SSO website.

So it would better if just use your old user login system but work together with Open SSO.

**9. Is it possible to use custom or other Database?**   
Open SSO using `Sequelize` libraries that work for multiple mainstream database. It is `SQLite`, `MySQL`, `MariaDB`, `PostgreSQL` and  `MSSQL`. If your custom database not supported by `Sequelize`, so the answer is not possible. You may need to rewrite the Open SSO source code by your self.

**10. Is it possible to run Open SSO on container cloud like docker or other SASS?**  
Yes it is possible, but the default Open SSO is not configured for it. You have to configured it by your self.

**11. Open SSO error when I try to update the package?**  
That will happen because you have been update also upgrade some dependencies. If you found an error, you should find what dependencies that trigger the error. I can help you to find out about this. Just feel free to contact me.  

To avoid this, you better to wait until I update the Open SSO in the future. Application that go for production, should be not too often to update. 

**12. My question is not here, can I contact you?**  
Sure, feel free to email me at [aalfiann@gmail.com](mailto:aalfiann@gmail.com).  
Or just chat with me via Telegram [https://t.me/aalfiann](https://t.me/aalfiann).

> [Back to top ^](#table-of-contents)
---
