---
title: "Open SSO Documentation"
date: 2022-12-04T18:58:26+07:00
lastmod: 2022-12-04T18:58:26+07:00
draft: false
author: "aalfiann"
authorLink: "https://linktr.ee/aalfiann"
authorEmail: "aalfiann@gmail.com"
keywords: "Open SSO Documentation"
images: ["/img/content/opensso.png"]

tags: ["opensso", "opensso-docs"]
categories: ["Documentation"]
featuredImage: "/img/content/opensso.png"
---
**Description**  

Open SSO is an User Management System with Single Sign On. Easier to manage your user and make them able to sign-in to all of your applications.  
<!--more-->

Open SSO was designed for high scalable purpose. Built with [Fastify Framework](https://fastify.io), one of the fastest JavaScript framework. Open SSO is very small application (only user management system) and isolated, so this will make you easier to integrate to your current web/application.

Open SSO use `denormalise` database to achieve high performance speed for `read` and `write` also to make easier for migration to `clustered` or `sharding` database. To reduce the request into database, there is built-in cache using `memory` as default but easily to swith to `redis` in the future.   

About authentication security, we follow the JWT standard `Oauth 2.0` as describe in [RFC7519](https://www.rfc-editor.org/rfc/rfc7519) and [RFC9068](https://datatracker.ietf.org/doc/html/rfc9068). Very fast performance because everything is stateless.  

---

#### Documentation
Below here is the full documentation about Open SSO project.  

#### Table Of Contents
- **[Get Started](#get-started)**
  - [A. Requirement](#a-requirement)
  - [B. Restore Database](#b-restore-database-optional-you-can-skip-this-part)
  - [C. Build and Run App](#c-build-app-and-run-it)
  - [D. Update](#d-update)
  - [E. Deploy to Server](#e-deploy-to-server)
  - [F. Create Admin User](#f-create-admin-user)
- **[Help](#help)**
- **[Credits](#credits)**
- **[Changelog](#changelog)**

#### Learn more
- **[Open SSO Advanced Configuration](/posts/open-sso-advanced-configuration)**
- **[Open SSO API Documentation](/posts/open-sso-api-documentation)**
- **[How to Integrate Open SSO](/posts/how-to-integrate-open-sso)**

---

## Get Started
Open SSO is a NodeJS application. Make sure you've done already to install the NodeJS server on your server or on your local computer.

### A. Requirement
Minimum requirement to install Open SSO project:
- NodeJS min v.14.19.3 - [Download NodeJS here](https://nodejs.org/dist/v14.19.3)
- Database [Optional]
- Redis 3 [Optional]

**Note:**
- The default database is using SQLite, but you can switch to another database like MariaDB, MySQL or PostgreSQL for easy monitoring, maintaining and scaling in the future.
- The default cache engine is using memory, but you can switch to Redis easily from config file. Using Redis is optional but it will become required if your application working on container service, clustered or behind the proxy load balancer.

> [Back to top ^](#table-of-contents)
---

### B. Restore Database [Optional, you can skip this part]
This is optional, Open SSO using SQLite as default and all features works well with it. You need to setup database if you have a plan to run Open SSO using MariaDB or MySQL database server. You can skip this if `SQLite` is enough for you. 

1. Install `MariaDB` or `MySQL`. You can search how to install this by using Google search engine.
2. Create a New Database, then execute or import the file `db.mysql.sql` located inside directory `database` into your database.  
  See the picture below.  
  ![](https://i.imgur.com/CGBVayx.png)

3. Edit `config.js` like this below
![](/img/content/tutorial/config-mariadb.png)

4. You have to install one of this package.
- MySQL >> npm install mysql2
- MariaDB >> npm install mariadb
- PostgreSQL >> npm install pg pg-hstore
- MSSQL >> npm install tedious
- Oracle >> npm install oracledb

5. Done

**Note:**
- File `db.mysql.sql` is only works for `MariaDB` and `MySQL` only.
- File `db.postgre.sql` is only works for `PostgreSQL` only.
- Working with `MSSQL` or `Oracle` should works too, but we don't create the database structure for it, so you have to create it by yourself.

> [Back to top ^](#table-of-contents)
---

### C. Build App and Run it
You have to build the app for the first time.  
1. Go to the directory path then typing
    ```
    npm install
    ```
2. Wait until the process complete.
3. Then you can run the app by typing
    ```
    node server.js
    ```

4. Now, Open the url [http://localhost:3000](http://localhost:3000).

**Note:**
- There is no default account, so when you're running this application for the first time, you have to register a new account to get the `admin` role.

> [Back to top ^](#table-of-contents)
---

### D. Update
Open SSO is using many dependencies, please try to update some security patch before you deploy Open SSO to your server. This would be better if you repeat doing this periodically for every 6 months.

#### Update App
1. Go to the directory path then typing
    ```
    npm update
    ```
2. Done

> [Back to top ^](#table-of-contents)
---

### E. Deploy to Server
If you finished doing from point A to D, now we prepared to deploying to server. In this case, I'll using `cPanel` and `SQLite` database because it's very cheap and easy.

#### a. Preparation
1. Edit the `config.js file`, then change this part with your domain or subdomain, in this case I'll using my subdomain `sso.sail-it.id`.
![](/img/content/tutorial/config-default.png)  
2. Then save it.
3. Now select all files then compressed to zip, give it name `app.zip`.
4. Done.

#### b. Upload to hosting
1. Go to your `cPanel` and look for `File Manager`.
2. Create new folder name `app` on root.  
![](/img/content/tutorial/cpanel-1.png)  
3. Go inside app folder, then click `Upload` button at top left.  
Then you will being redirected to the new page for upload, now choose your zip file and start upload it.  
![](/img/content/tutorial/cpanel-2.png)  
When the upload complete, you can close it page and back to `File Manager`.
4. Click `Reload` button then you will see your `app.zip` file on there.  
Now right click on that file, then choose `Extract`.    
![](/img/content/tutorial/cpanel-3.png)  
Just type `/app` on it, then click `Extract Files`.  
![](/img/content/tutorial/cpanel-4.png)  
When the extract successfuly, it will shown like this below  
![](/img/content/tutorial/cpanel-5.png)  
Just click `Close` and click `Reload` button again. Then you will see app files already extracted.
![](/img/content/tutorial/cpanel-6.png)
5. Done, you can close the `File Manager` page.

#### c. Setup NodeJS App
1. Go to your `cPanel` again.
2. Look for `Setup NodeJS App` then click `CREATE APPLICATION` button.  
![](/img/content/tutorial/cpanel-7.png)
3. Configuration should look like this below, then click `CREATE` button
![](/img/content/tutorial/cpanel-8.png)
4. Then click `Start App`, if there is no error, the button will display like this below
![](/img/content/tutorial/cpanel-9.png)
5. Done, now try to visit the website.  
![](/img/content/tutorial/cpanel-11.png)

> [Back to top ^](#table-of-contents)
---

### F. Create Admin User
1. Just go to `Sign Up` page.
2. Fill the form and click `Sign Up` button.  
![](/img/content/tutorial/cpanel-12.png)
3. Done, the first user will get admin user automatically.

> [Back to top ^](#table-of-contents)
---

## Help
If this documentation didn't help to your spesific problem, just feel free sending email to aalfiann@gmail.com.  
We always try to update this documentation in the future.

> [Back to top ^](#table-of-contents)
---

## Credits
All Open SSO libraries or external scripts is using an open source library.

Frontend we using:
- [Argon Dashboard 2](https://www.creative-tim.com/product/argon-dashboard)
- [Bootstrap 5](https://getbootstrap.com)
- [reCaptcha V3](https://developers.google.com/recaptcha/docs/v3)
- [Dom.js](https://github.com/aalfiann/dom.js)
- [Axios Http](https://axios-http.com)
- [Browser Storage Class](https://gitlab.com/aalfiann/browser-storage-class)
- [Fly Json ODM](https://github.com/aalfiann/fly-json-odm)
- [Momentjs](https://momentjs.com)
- [Native Form Validation](https://github.com/aalfiann/native-form-validation)
- [ReefJS V12](https://reefjs.com)
- [Sweetalert](https://sweetalert.js.org)

**Note:**
- For backend, you can see the dependencies at `package.json`.
- There is no jQuery in this application, but you are able to add it by yourself if you need it. 

> [Back to top ^](#table-of-contents)

## Changelog
#### v1.0.0 - 03/2023

- First Release

> [Back to top ^](#table-of-contents)
---
