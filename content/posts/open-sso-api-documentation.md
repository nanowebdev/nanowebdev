---
title: "Open SSO API Documentation"
date: 2022-12-02T17:58:26+07:00
lastmod: 2022-12-02T17:58:26+07:00
draft: false
author: "aalfiann"
authorLink: "https://linktr.ee/aalfiann"
authorEmail: "aalfiann@gmail.com"
keywords: "Open SSO API Documentation"

tags: ["opensso", "opensso-docs"]
categories: ["Documentation"]
---
Open SSO basically is a Rest API first system. Here is the fundamental API list to use in your website.  
<!--more-->

#### Table Of Contents
**[X-Token](#x-token)**
  - [1. Login Normal](#1-login-normal)
  - [2. Login via Oauth](#2-login-via-oauth)
  - [3. Login via SSO Login Page](#3-login-via-sso-login-page)

**[API](#api)**
  - [A. Verify Token](#a-verify-token)
  - [B. Get My Profile](#b-get-my-profile)
  - [C. Update My Profile](#c-update-my-profile)
  - [D. Deactivate My Profile](#d-deactivate-my-profile)
  - [E. Get Another User Profile](#e-get-another-user-profile)
  - [F. API via Postman](#f-api-via-postman)

**[Response Format](#response-format)**

#### Learn more
- **[Open SSO Get Started](/posts/open-sso-documentation)**
- **[Open SSO Advanced Configuration](/posts/open-sso-advanced-configuration)**
- **[How to Integrate Open SSO](/posts/how-to-integrate-open-sso)**
---

## X-Token
X-Token is a header that should be filled with JWT token generated from user login.  
Most API request in Open SSO will require `X-Token` header.  
You can get X-Token in three different ways.

### 1. Login Normal
Get X-Token via login normal.


|Method|Endpoint|Header|Body|
|---|---|---|---|
|POST|http://localhost:3000/api/user/login|Content-Type: application/json|{"username": "", "password": ""}|

Example output response
```json
{
    "message": "Login user success!",
    "statusCode": 200,
    "success": true,
    "token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI1ZDM3NDk5Yi0xM2ZjLTQzZmItOWE3Yi1kZGI3Nzc1N2FjOTMiLCJ1bm0iOiJhYWxmaWFubiIsIm5hbWUiOiJhYWxmaWFubiIsIm1haWwiOiJhYWxmaWFubkBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJncmF2YXRhciI6Imh0dHBzOi8vZ3JhdmF0YXIuY29tL2F2YXRhci82ZDdiZDI0MWEyNjc5ZDc1NTc4OTQwMjY4MWQyNWVjMSIsImhhc2giOiJVeHlNVGJqb0cxcHBoWnpNVE1UUSIsImlhdCI6MTY3Mjc3NDg3MCwiZXhwIjoxNjcyODAzNjcwfQ.I1OZASUEtLOBYcaKLHesbsIfywQmB00e1mQO2QTroGmPQtlA84mqwUDYbAiOhS0micIilMlSaIYk0_QdwVABCWNqJlREU0BeQkrKE64tuJ4WHY7lbTvklPOA0k-j6JBZpqp-D0qw8F8-zN6iYq0vyuBHZ6vfRe-0i-B96FiijKZKPkG3KPmXnY0HJtx4GPxhigaUvLb2rRN4eV8Am--2XnbAWpRqecuI5LM6b2oFG-ZoOfNm0LLNe2TJCaKUXBFvoAIZnwAk_l0HZmu-wtiCjkmcjQ5aOO1mrZTONAckvfVFWjK4bx4IFXFeLUVZPOJA3_z9PzH--AoYzjQkYL_q8Q",
    "expire": 1672803670000
}
```

> [Back to top ^](#table-of-contents)
---

### 2. Login via Oauth
User who registered using oauth Google or Apple will require this API.  

**Note:**
- This method will auto register new user if an username or email not exists inside OpenSSO.
- You should follow the oauth flow Google and Apple or bad person could spamming your application.
- If you not sure or can't follow the oauth Google and Apple, you better use `Login via SSO Login Page`.

#### a. Get Access Token
You need to have `SSO Key`, by create your SSO inside menu `My SSO`.  

See picture below  
![](/img/content/doc-opensso/sso-key.png)

|Method|Endpoint|Header|Body|
|---|---|---|---|
|GET|http://localhost:3000/api/oauth/request_token|Content-Type: application/json, Access-Key: {{YOUR_SSO_KEY}}||

Example output response
```json
{
    "message": "Access token successfully generated!",
    "statusCode": 200,
    "success": true,
    "access_token": "=Ng=zY3MzNzOjMmY2YzdmYjODNGRxxhNhN5NjZjQGRiZmMkNDYTNmExQ1OxOTxxx"
}
```

**Note:**
- `SSO Key` have no expiring time, but you can't use it again if its status disabled or removed from `My SSO` menu.

#### b. Get X-Token
Once you have the `access-token`, you can use it to request `X-Token`.

**Google**  
|Method|Endpoint|Header|Body|
|---|---|---|---|
|POST|http://localhost:3000/api/oauth/google|Content-Type: application/json, Access-Token: {{YOUR_ACCESS_TOKEN}}|{"username":"yourusername", "email":"youremail@gmail.com","gravatar":""}|

**Apple**  
|Method|Endpoint|Header|Body|
|---|---|---|---|
|POST|http://localhost:3000/api/oauth/apple|Content-Type: application/json, Access-Token: {{YOUR_ACCESS_TOKEN}}|{"username":"yourusername", "email":"youremail@gmail.com"}|

Example output response
```json
{
    "message": "Oauth google success!",
    "statusCode": 200,
    "success": true,
    "token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJiOGEwMzlmNy1jZjUyLTQ0MDktODU3OS01Yzc0Yjk3MzI2MDMiLCJ1bm0iOiJhYWxmaWFubiIsIm5hbWUiOiIiLCJtYWlsIjoiYWFsZmlhbm5AZ21haWwuY29tIiwiem9uZSI6IiIsInJvbGUiOiJtZW1iZXIiLCJncmF2YXRhciI6Imh0dHBzOi8vZ3JhdmF0YXIuY29tL2F2YXRhci82ZDdiZDI0MWEyNjc5ZDc1NTc4OTQwMjY4MWQyNWVjMSIsImhhc2giOiJBPT1RNE82TlRaWElXMWlwdFoyTUROalEiLCJpYXQiOjE2OTA0MzQ4MTEsImV4cCI6MTY5MDQ2MzYxMX0.XPTmCkJn85e26vgtnMdU5yLi6TGcBu0ct_MpVniJw8dB6yrxn3cNZm9luNJnSh7rWNLZV40jOYS0FbOxMMxfpF-n9uNUhoMnWqwd9pO3VERDuXOlhXVeWhHRHfn87JV2w_mlI6ZFfA3t3WjT5D8cydzs1Hf1Y8l726mVwrXMxJgV0W8TTIz1BiEPEnsFTX4V_fS5IdStz_8rz43q7keeUgBZ0qlGULjQpYGI_6xIKqsY1AP2rrQ9BJ57QL71aoULAJ5Uu7eTCicnO77pRJO-Hi2xFoZpIabsAZfOyd01hTnk3htsolIDY-_dB1_t5Hruullu89oNAWcgK_bChXzLuw",
    "expire": 1690463611000
}
```

### 3. Login via SSO Login Page
The simpler and safer is just use SSO Login Page to get the `X-Token`. But there is some tricky because Google Login Button can't displayed in Android WebView as default.


> [Back to top ^](#table-of-contents)
---


## API
If you already have the `X-Token`, then you can access all APIs below here.

### A. Verify Token
When you have the new token, you shouldn't need to verify it all the time. This API is created just for test only.


|Method|Endpoint|Header|Body|
|---|---|---|---|
| POST | http://localhost:3000/api/auth/verify | X-Token: {{YOUR_TOKEN}} | |

Example output response
```json
{
    "message": "Verify authentication success!",
    "statusCode": 200,
    "jwt": {
        "uid": "5d37499b-13fc-43fb-9a7b-ddb77757ac93",
        "unm": "aalfiann",
        "name": "aalfiann",
        "mail": "aalfiann@gmail.com",
        "role": "admin",
        "gravatar": "https://gravatar.com/avatar/6d7bd241a2679d755789402681d25ec1",
        "hash": "UxyMTbjoG1pphZzMTMTQ",
        "iat": 1672774870,
        "exp": 1672803670
    }
}
```

**Explanation**  
- `uid` is your user id.
- `unm` is your username.
- `name` is your fullname.
- `mail` is your email address.
- `role` is the level of user.
- `gravatar` is the global avatar of user.
- `hash` is the random generated string, only used for internal system.

Below here is the JWT standard which is described in [RFC7519](https://www.rfc-editor.org/rfc/rfc7519): 
- `iat` is identifies the time at which the JWT was issued.
- `exp` is expiration time for JWT.


**Note:**
- As alternative, you are able to verify your token through [JWT.io](https://jwt.io).
- To make the signature valid on [JWT.io](https://jwt.io), you have to paste your `private.key` and `public.key` to its website.

> [Back to top ^](#table-of-contents)
---

### B. Get My Profile
This API is to show your user information


|Method|Endpoint|Header|Body|
|---|---|---|---|
| POST | http://localhost:3000/api/user/my-profile | X-Token: {{YOUR_TOKEN}} | |

Example output response
```json
{
    "message": "Get my profile successfully!",
    "statusCode": 200,
    "success": true,
    "data": {
        "created_at": 1666643963244,
        "modified_at": 1666643963244,
        "id": "5d37499b-13fc-43fb-9a7b-ddb77757ac93",
        "username": "aalfiann",
        "fullname": null,
        "email": "aalfiann@gmail.com",
        "role": "admin",
        "method": "internal",
        "service": "internal",
        "data": null,
        "status": 1,
        "created_at_month": "October",
        "created_at_year": 2022,
        "gravatar": "https://gravatar.com/avatar/6d7bd241a2679d755789402681d25ec1"
    }
}
```

> [Back to top ^](#table-of-contents)
---

### C. Update My Profile
This API is to modify your user information


|Method|Endpoint|Header|Body|
|---|---|---|---|
| POST | http://localhost:3000/api/user/my-profile/update | X-Token: {{YOUR_TOKEN}}, Content-Type: application/json | {"fullname": "M ABD AZIZ ALFIAN", "email": "aalfiann@gmail.com", "data": {"address": "Jakarta Timur DKI Jakarta", "about_me": "Just ordinary developer"}} |

Example output response
```json
{
    "message": "Your profile successfully updated!",
    "statusCode": 200,
    "success": true,
    "data": [
        1
    ]
}
```

**Note:**
- Field `data` is an object type, you can set whatever information on your data user.

> [Back to top ^](#table-of-contents)
---

### D. Deactivate My Profile
This API is to deactivate or delete your account.

|Method|Endpoint|Header|Body|
|---|---|---|---|
| POST | http://localhost:3000/api/user/my-profile/suspend | X-Token: {{YOUR_TOKEN}}, Content-Type: application/json | {"username": "aalfiann"} |

Example output response
```json
{
    "message": "Your profile successfully suspended!",
    "statusCode": 200,
    "success": true,
    "data": [
        1
    ]
}
```

**Note:**
- This action will make your profile deactivated and permanently suspended.
- Nobody can't find you anymore.
- Your username will not available anymore (nobody can't use your username).

> [Back to top ^](#table-of-contents)
---

### E. Get Another User Profile
This API is to show another user information.

|Method|Endpoint|Header|Body|
|---|---|---|---|
| GET | http://localhost:3000/api/user/profile/<<username>> | | |

Example output response
```json
{
    "message": "Get user profile successfully!",
    "statusCode": 200,
    "success": true,
    "data": {
        "created_at": 1666643963244,
        "modified_at": 1672776525314,
        "id": "5d37499b-13fc-43fb-9a7b-ddb77757ac93",
        "username": "aalfiann",
        "fullname": "M ABD AZIZ ALFIAN",
        "method": "internal",
        "service": "internal",
        "data": {
            "address": "Jakarta Timur DKI Jakarta",
            "about_me": "Just ordinary developer"
        },
        "status": 1,
        "created_at_month": "October",
        "created_at_year": 2022,
        "gravatar": "https://gravatar.com/avatar/6d7bd241a2679d755789402681d25ec1"
    }
}
```

**Note:**
- Replace `<<username>>` with the username of other user.
- This is public API, so you can request it without X-Token header. 

> [Back to top ^](#table-of-contents)
---

### F. API via Postman
There is 50 API used in Open SSO. Many of them are used for internal system. You can learn it or make an experiment directly using `Postman`. Just import the file `postman_collection.json` (located at root directory) to your Postman Application.

> [Back to top ^](#table-of-contents)
---

## Response Format
Here is the default response format, any request will having `message` and `statusCode` key properties. The `error` key property will shown only at `4xx` and `5xx` http status code.

- **Success**
```json
{"message": "string", "statusCode": 200}
```
- **Created**
```json
{"message": "string", "statusCode": 201}
```
- **Bad Request**
```json
{"message": "string", "statusCode": 400, "error": "Bad Request"}
```
- **Unauthorized**
```json
{"message": "string", "statusCode": 401, "error": "Unauthorized"}
```
- **Forbidden**
```json
{"message": "string", "statusCode": 403, "error": "Forbidden"}
```
- **Not Found**
```json
{"message": "string", "statusCode": 404, "error": "Not Found"}
```
- **Internal Server Error**
```json
{"message": "string", "statusCode": 500, "error": "Internal Server Error"}
```

> [Back to top ^](#table-of-contents)
---
