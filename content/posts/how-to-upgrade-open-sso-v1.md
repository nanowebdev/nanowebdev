---
title: "How to Upgrade Open SSO v1.x.x"
date: 2023-07-27T13:50:26+07:00
lastmod: 2023-07-27T13:50:26+07:00
draft: false
author: "aalfiann"
authorLink: "https://linktr.ee/aalfiann"
authorEmail: "aalfiann@gmail.com"
keywords: "How to Upgrade Open SSO v1.x.x"

tags: ["opensso", "opensso-docs"]
categories: ["Documentation"]
---
Here is the Open SSO changelog information also How to take a step for upgrade Open SSO v1.x.x.    
<!--more-->

#### Table Of Contents

**[Changelog](#changelog)**
  - [v1.0.0](#v100)
  - [v1.1.0](#v110)

**[Upgrade](#upgrade)**
  - [v1.0.0 - v1.1.0](#v100---v110)
---

## Changelog

### v1.0.0
Date Release: 20 Mar 2023
 - [x] First Release

### v1.1.0
Date Release: 01 Aug 2023
- [x] Add Oauth for Apple ID.
- [x] Update email for reset password.
- [x] Update config.
- [x] Update dependencies.
- [x] Update postman file.
- [x] Update docs about API.
- [x] Add docs how to upgrade.

> [Back to top ^](#table-of-contents)
---

## Upgrade

### v.1.0.0 - v.1.1.0
**File Changes**  
This upgrade theres some files has changed. Just replace it but if you have modified OpenSSO code, you should consider about these files:
- routes/oauth.js
- routes/user.js
- schemas/oauth.js
- views/default/email-reset.html
- views/default/sign-in.html
- views/default/sso-login.html
- changelog.md
- config.default.js
- config.js
- postman_collection.json
- package-lock.js
- package.js


**Add new config.js**  
There is new configuration for `oauth`.
```js
oauth: {
  google: {
    // ...
  },
  apple: {
    enable: false,
    clientId: 'com.xxx.login',
    redirectURI: 'https://yourdomain.com'
  }
}
```


**Upgrade dependencies**  

1. Uninstall old package
```bash
npm remove @fastify/view eta sqlite3 sequelize fastify
```

2. Install new package
```bash
npm install @fastify/view@7 eta@2 sqlite3 sequelize fastify
```

3. Audit Fix
```bash
npm audit fix
```

4. Done

As you can see, there still 3 package left can't be upgraded because there is still no fix package available, since this is moderate status, we can ignore this for now.


> [Back to top ^](#table-of-contents)
---

