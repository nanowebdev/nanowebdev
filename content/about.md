---
title: "About NanoWebDev"
date: 2020-03-04T15:58:26+08:00
lastmod: 2020-03-04T15:58:26+08:00
draft: false
author: "aalfiann"
authorLink: "https://linktr.ee/aalfiann"
authorEmail: "aalfiann@gmail.com"
keywords: "Tester build small applications"
description: ""
images: ["/img/about/microservices-diagram.jpg"]
---

We build small applications for horizontal scaling purpose.

### Our Goals
Our goal is to build high scalable application with microservices architecture. To achieve this, we have to make our application smaller as service.  

We also trying to make our application could running as standalone (not dependency with others app). So you can use our application in two environment, as monolith or microservices.

![](/img/about/microservices-diagram.jpg)
_Above image only an illustrations how micorservices work_

### Our Roadmap
Note: _Checklisted means the project is done._

- [x] **[Single Sign ON](https://codecanyon.net/item/open-sso-single-sign-on-nodejs/44234269)**  
  This is a service / app to allow your user to sign-in into all applications.

- [ ] **[File Storage](#)**  
  This is a service / app for file management system.

- [ ] **[CMS System](#)**  
  This is a service / app for content management system.

- [ ] **[Comment System](#)**  
  This is a service / app for comment system.

- [ ] **[Ticket Support](#)**  
  This is a service / app for ticket support system.

- [ ] **[Short URL](#)**  
  This is a service / app for url management system.

- [ ] **[Wallet System](#)**  
  This is a service / app for money wallet system.

### Who are we ?
NanoWebDev is a non-profit organizations. We are try to building a small application that efficient and can scale for enterprise business solution. We are selling the applications with cheap price, so only enough to buy us some coffee and our time efforts.

Currently NanoWebDev don't have a team, still managed by single developer:  
- [M ABD AZIZ ALFIAN](https://linktr.ee/aalfiann) as Lead Developer
- ...
- ...

Hope in the future NanoWebDev will growth as big developer company.  
Thank you for buying our applications.
